const axios = require('axios')
const fs = require('fs')
const readline = require('readline')
require('dotenv').config()

const riotKey = process.env.RIOT_KEY
// All regions: ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const region = ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const getPuuid = async () => {
    for await (const r of region){
        var rl = readline.createInterface({
            input: fs.createReadStream(`./players/${r}.players`)
          });
        let stream=fs.createWriteStream(`./puuid/${r}.puuid`)
        stream.on('error',console.error)
        for await (const line of rl){
            const str=line.replace(' ','%20')
            axios.get(`https://${r}.api.riotgames.com/lol/summoner/v4/summoners/by-name/${str}?api_key=${riotKey}`)
            .then((response)=>{
                const {puuid,name} = response.data
                stream.write(puuid+'\n')
                console.log(`${name}'s puuid obtainded!`)
            })
            .catch((error)=>{
                console.log(`${line}'s puuid could't be obtained - error `)
            })
            await new Promise(r => setTimeout(r, 1300));
        }
        stream.end()
        console.log(`Process complete for region ${r}`)
    }
}
const intersection = (array1,array2)=>{
    const aux = array1.filter(element => array2.includes(element));
    if (aux.length==0){
        return false
    }
    return true
} 
getPuuid()