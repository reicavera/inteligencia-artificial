const axios = require('axios')
const fs = require('fs')
const readline = require('readline')
const { nameToInt, roleToInt } = require('./normalize')
require('dotenv').config()

const riotKey = process.env.RIOT_KEY
// All servers: ['americas','asia','europe']
const server = ['asia']
const defineRole = (champions)=> {
    const roles = ['TOP', 'JUNGLE', 'MIDDLE', 'BOTTOM', 'UTILITY']
    for(c of champions){
        if(roles.includes(c.role)){
            roles.pop(c.name)
        }
    }
    return roles[0]
}
const filter = async () => {
    for (const s of server){
        const queue=[]
        var rl = readline.createInterface({
            input: fs.createReadStream(`./data/${s}.json`)
          });
        var stream=fs.createWriteStream(`./clean_data/${s}.json`)
        stream.on('error',console.error)
        await stream.write('[')
        var i = 0
        const max = 30000
        for await (const line of rl){
            if(i>=max){
                continue
            }
            var match = await JSON.parse(line.slice(0,-1))
            if(!match.blue.win&&!match.red.win){
                continue
            }
            if(!match.blue.win&&!match.red.win){
                continue
            }
            if(queue.includes(line)){
                continue
            }
            if(queue.length==5){
                queue.shift()
                queue.push(line)
            }
            let flawed = false
            for (c of match.blue.champions){
                c.name=nameToInt(c.name)
                c.role=roleToInt(c.role)
                if(c.name == undefined || c.role == undefined){
                    flawed=true
                }
            }
            for (c of match.red.champions){
                c.name=nameToInt(c.name)
                c.role=roleToInt(c.role)
                if(c.name == undefined || c.role == undefined){
                    flawed=true
                }
            }
            if(flawed){
                continue
            }
            i++
            match.blue.champions.sort(compare)
            match.red.champions.sort(compare)
            await stream.write(JSON.stringify(match.blue.champions)+',\n')
            if(i!=max){
                await stream.write(JSON.stringify(match.red.champions)+',\n')
            }
            else{
                await stream.write(JSON.stringify(match.red.champions)+']')
            }
        }
        stream.end()
        console.log(`Process complete for server ${s}`)
    }
}
filter()

function compare( a, b ) {
    if ( a.role < b.role ){
      return -1;
    }
    return 0;
  }