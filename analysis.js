const {makeMatrix} = require('make-matrix')
const {number,intToName, nameToInt} = require('./normalize')
const prompt = require('prompt-sync')()
// All regions: ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const region = ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']

const data =[]
for(const r of region){
  const array = require(`./data/${r}.json`) 
  for(const v of array){
    data.push(v)
  } 
}

const table = makeMatrix([number,5], 0)

shuffle(data)
const rate = 0.75
const separator = Math.round(data.length*rate)
const train = data.slice(0,separator)
const test = data.slice(separator)
console.log(`Data size: ${data.length}, Train rate: ${rate}, TestRate: ${1-rate}`)

// Train phase
for(const team of train){
  for(const champion of team){
    table[champion.name][champion.role]++
  }
}
console.log(`Training ended!`)
let right = 0

// Test phase
for(const team of test){
  let shuffledTeam = JSON.parse(JSON.stringify(team));
  shuffle(shuffledTeam)
  const answer = predict(shuffledTeam)
  if(JSON.stringify(answer) === JSON.stringify(team)){
    right++
  }
}
console.log(`Test Ended!`)

//Shows success rate
percentage = (100*right/test.length).toFixed(2)
console.log(`Success rate: ${percentage}%`)

//Additional info
let input = prompt('Wanna see content table (yes/no)? ')
if(input == 'yes'){
  for(const i in table){
    let count = 0
    for(const j in table[i]){
      count += table[i][j]
    }
    console.log(`${intToName(i)} - TOP: ${table[i][0]}, JG: ${table[i][1]}, MID: ${table[i][2]}, ADC: ${table[i][3]}, SUP: ${table[i][4]} | TOTAL: ${count}`)
  }
}

input = prompt('Wanna get a prediction of your team? (yes/no)? ')
while(input === 'yes'){
  const list  = []
  let i =1
  while(i<6){
    const name = prompt(`Enter champion ${i}: `)
    const int = nameToInt(name)
    if (int === -1){
      console.log('Champion name not found. Try again')
    }
    else{
      list.push({name:int})
      i++
    }
    
  }
  const answer = predict(list)
  console.log(`TOP: ${intToName(answer[0].name)}, JG: ${intToName(answer[1].name)}, MID: ${intToName(answer[2].name)}, ADC: ${intToName(answer[3].name)}, SUP: ${intToName(answer[4].name)}`)
  input = prompt('Wanna get a prediction of your team? (yes/no)? ')
}
// function to shuffle an array
function shuffle(array){
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}

//permutations of an array
function permutation(array) {
  function p(array, temp) {
      var i, x;
      if (!array.length) {
          result.push(temp);
      }
      for (i = 0; i < array.length; i++) {
          x = array.splice(i, 1)[0];
          p(array, temp.concat(x));
          array.splice(i, 0, x);
      }
  }

  var result = [];
  p(array, []);
  return result;
}

function predict(team){
  const list = []
  for(const champion of team){
    list.push(champion.name)
  }
  const p = permutation(list)
  let argmax = 0
  let max = 0
  for (const i in p){
    let value = 0
    for (const j in p[i]){
      value += Math.log(table[p[i][j]][j]+1)
    }
    if (value > max){
      max = value
      argmax = i
    }
  }
  prediction = p[argmax]
  const answer = []
  for(const i in prediction){
    answer.push({name:prediction[i],role:parseInt(i)})
  }
  return answer
}