const axios = require('axios')
const fs = require('fs')
const readline = require('readline')
require('dotenv').config()

const riotKey = process.env.RIOT_KEY
// All servers: ['americas','asia','europe']
const server = ['americas']
const filter = async () => {
    for (const s of server){
        const set = new Set()
        var rl = await readline.createInterface({
            input: fs.createReadStream(`./match/${s}.match`)
          });
        for await (const line of rl){
            set.add(line)
        }
        let stream=fs.createWriteStream(`./filter/${s}.filter`)
        stream.on('error',console.error)
        for(const e of set){
            stream.write(e+'\n')
            }
        stream.end()
        console.log(`Process complete for server ${s}`)
    }
}
filter()