const axios = require('axios')
const fs = require('fs')
require('dotenv').config()
const riotKey = process.env.RIOT_KEY
let vars = process.argv.slice(2)
const getPlayers = async () => { 
    for await (const r of vars){
        let stream=fs.createWriteStream(`./players/${r}.players`)
        stream.on('error',console.error)
        await axios.get(`https://${r}.api.riotgames.com/lol/league/v4/challengerleagues/by-queue/RANKED_SOLO_5x5?api_key=${riotKey}`)
        .then((response)=>{
            const {entries} = response.data
            for(const element of entries){
              const str=element.summonerName
              stream.write(str+'\n')
            }
            console.log(`Process complete for region ${r}`)
        })
        stream.end()
    }
}

getPlayers()