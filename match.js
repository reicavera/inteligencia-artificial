const axios = require('axios')
const fs = require('fs')
const { get } = require('http')
const readline = require('readline')
require('dotenv').config()

const riotKey = process.env.RIOT_KEY
// All regions: ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const region = ['oc1']
const getMatch = async () => {
    for await (const r of region){
        let i=0
        var rl = readline.createInterface({
            input: fs.createReadStream(`./puuid/${r}.puuid`)
          });
        let stream = fs.createWriteStream(`./match/${r}.match`)
        stream.on('error',console.error)
        for await (const line of rl){
            i++
            axios.get(`https://${getServer(r)}.api.riotgames.com/lol/match/v5/matches/by-puuid/${line}/ids?type=ranked&start=0&count=100&api_key=${riotKey}`)
            .then((response)=>{
                const {data} = response
                for(const d of data){
                    stream.write(d+'\n')
                }
                console.log(`${i}'s matches obtainded!`)
            })
            .catch((error)=>{
                console.log(`${i}'s matches could't be obtained - error` )
            })
            await new Promise(r => setTimeout(r, 1300));
        }
        stream.end()
        console.log(`Process complete for region ${r}`)
    }
}
const getServer = (region) =>{
    switch(region){
        case 'br1':
        case 'la1':
        case 'la2':
        case 'na1':
            return 'americas'
        case 'eun1':
        case 'euw1':
        case 'tr1':
        case 'ru':
            return 'europe'
        case 'jp1':
        case 'kr':
            return 'asia'
        case 'oc1':
            return 'sea'
    }
}
getMatch()