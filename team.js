const axios = require('axios')
const fs = require('fs')
const readline = require('readline')
require('dotenv').config()

const riotKey = process.env.RIOT_KEY
// All regions: ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const region = ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const getTeam = async () => {
    for await (const r of region){
        const read=fs.createReadStream(`./team/${r}.team`)
        var aux = readline.createInterface({
            input: read
          })
        var n=0
        for await(const line of aux){
            n++
        }
        aux.close();
        read.destroy();
        var rl = readline.createInterface({
            input: fs.createReadStream(`./filter-match/${r}.match`)
          })
        let stream=fs.createWriteStream(`./team/${r}.team`,{flags:'a'})
        stream.on('error',console.error)
        var i=0
        for await (const line of rl){
            const iCopy=i++
            if(i<=n){
                continue
            }
            axios.get(`https://${getServer(r)}.api.riotgames.com/lol/match/v5/matches/${line}?api_key=${riotKey}`)
            .then((response)=>{
                const {participants} = response.data.info
                const blue = []
                const red = [] 
                for (const p of participants){
                    const {championName,teamId,teamPosition,} = p
                    const champion={
                        name:championName,
                        role:teamPosition
                    }
                    if(teamId==100){
                        blue.push(champion)
                    }
                    else{
                        red.push(champion)
                    }    
                }
                const match={blue,red}
                stream.write(JSON.stringify(match)+'\n')
                console.log(`${iCopy} - ${line}'s match obtainded!`)
            })
            .catch((error)=>{
                if(error.response.status!=404){
                    console.log(`${iCopy} - ${line}'s match info not obtained - error`)
                    flag=true
                }
            })
            await new Promise(r => setTimeout(r, 1300));
        }
        stream.end()
        console.log(`Process complete for region ${r}`)
    }
}
function getServer(region){
    switch(region){
        case 'br1':
        case 'la1':
        case 'la2':
        case 'na1':
            return 'americas'
        case 'eun1':
        case 'euw1':
        case 'tr1':
        case 'ru':
            return 'europe'
        case 'jp1':
        case 'kr':
            return 'asia'
        case 'oc1':
            return 'sea'
    }
}
getTeam()