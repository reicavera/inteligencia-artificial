const fs = require('fs')
const readline = require('readline')
const { nameToInt, roleToInt } = require('./normalize')
require('dotenv').config()

const riotKey = process.env.RIOT_KEY
// All regions: ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const region = ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const filter = async () => {
    for (const r of region){
        const queue=[]
        var rl = readline.createInterface({
            input: fs.createReadStream(`./team/${r}.team`)
        });
        var stream=fs.createWriteStream(`./data/${r}.json`)
        stream.on('error',console.error)
        let str = '['
        for await (const line of rl){
            var match = await JSON.parse(line)
            if(match.blue.length != 5 || match.red.length != 5){
                continue
            }
            if(queue.includes(line)){
                continue
            }
            if(queue.length==5){
                queue.shift()
                queue.push(line)
            }
            let flawed = false
            for (c of match.blue){
                c.name=nameToInt(c.name)
                c.role=roleToInt(c.role)
                if(c.name == undefined || c.role == undefined){
                    flawed=true
                }
            }
            for (c of match.red){
                c.name=nameToInt(c.name)
                c.role=roleToInt(c.role)
                if(c.name == undefined || c.role == undefined){
                    flawed=true
                }
            }
            if(flawed){
                continue
            }
            match.blue.sort(compare)
            match.red.sort(compare)
            str +=JSON.stringify(match.blue)+',\n'
            str +=JSON.stringify(match.red)+',\n'
        }
        await stream.write(str.slice(0,-2)+']')
        stream.end()
        console.log(`Process complete for region ${r}`)
    }
}
filter()

function compare( a, b ) {
    if ( a.role < b.role ){
      return -1;
    }
    return 0;
  }