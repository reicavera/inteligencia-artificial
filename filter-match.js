const axios = require('axios')
const fs = require('fs')
const readline = require('readline')
require('dotenv').config()

const riotKey = process.env.RIOT_KEY
// All regions: ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const region = ['br1','eun1','euw1','jp1','kr','la1','la2','na1','oc1','ru','tr1']
const filter = async () => {
    for (const r of region){
        const set = new Set()
        var rl = await readline.createInterface({
            input: fs.createReadStream(`./match/${r}.match`)
          });
        for await (const line of rl){
            set.add(line)
        }
        let stream=fs.createWriteStream(`./filter-match/${r}.match`)
        stream.on('error',console.error)
        for(const e of set){
            stream.write(e+'\n')
            }
        stream.end()
        console.log(`Process complete for server ${r}`)
    }
}
filter()